abstract class Instrument 
{
    abstract void play();
}

class Piano extends Instrument
{
    @Override
    void play() 
    {
        // TODO Auto-generated method stub
        System.out.println("Piano is playing tan tan tan tan ");
    }
}

class Flute extends Instrument
{
    @Override
    void play() 
    {
        // TODO Auto-generated method stub
        System.out.println("Flute is playing toot toot toot toot");
    }
}

class Guitar extends Instrument
{
    @Override
    void play() 
    {
        // TODO Auto-generated method stub
        System.out.println("Guitar is playing tin tin tin");
    }
}

public class MainClassInstrument
{
    public static void main(String args[])
    {
        Instrument array[] = new Instrument[10];
        int i;
        for(i = 0; i < 10; i += 1)
        {
            array[i] = new Piano();

            if (i % 3 == 1)
                array[i] = new Flute();

            if (i % 3 == 2)
                array[i] = new Guitar(); 
        }

        for(i = 0; i < 10; i+=1)
        {
           System.out.print((i+1) + " ");
           array[i].play(); 
        }
    }
}






