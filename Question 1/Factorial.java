import java.util.Scanner;

class Factorial
{
    static int fact(int n)
    {
        if (n == 1)
            return 1;
        else
            return n * fact(n-1);
    }
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the value of N : ");
        int N = sc.nextInt();
        
        System.out.println("The factorial is of " + N +" is : " + fact(N));
    }
}