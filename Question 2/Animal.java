class animalClass 
{
    protected String shout;
    animalClass(String s)
    {
        this.shout = s;
    }
    String show() // overrided Method
    {
        return "Animal makes : ";
    }
}

class Cats extends animalClass
{
    Cats(String S)
    {
        super(S);
    }
    
    @Override
    String show()
    {  
        return super.show() + super.shout;
    }
}

class Dog extends animalClass
{
    Dog(String S)
    {
        super(S);
    }

    @Override
    String show()
    {  
        return super.show() + super.shout;
    }
}

class Horse extends animalClass
{
    Horse(String S)
    {
        super(S);
    }
    
    @Override
    String show()
    {  
        return super.show() + super.shout;
    }
}


public class Animal
{
    public static void main(String args[])
    {
        animalClass animal;
        
        animal = new Cats("Meow");
        System.out.println(animal.show());

        animal = new Dog("Bark");
        System.out.println(animal.show());

        animal = new Horse("Neigh");
        System.out.println(animal.show());
        
    }
}



