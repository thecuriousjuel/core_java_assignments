import java.util.ArrayList;
import java.util.Scanner;

public class Employee
{
    private String id, name, address;
    private float sal;

    Employee()
    {

    }

    public Employee(String id, String name, String address, float sal)
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.sal = sal;
    }

    String search(String id, String name, ArrayList<Employee> employee)
    {
        for(Employee emp : employee)
        {
            if (emp.id.equals(id) && emp.name.equals(name))
            {
            	System.out.println("Found!");
                return toString(emp);
            }
        }
        return "Not Found";
    }

    String toString(Employee employee)
    {
        return "Id : " + employee.id +"\tName : " + employee.name +
                "\tAddress : " + employee.address + "\tSalary : "+ employee.sal;
    }

    void show(ArrayList<Employee> employee)
    {
    	System.out.println("----Displaying all the entered employee details below----");
    	for(Employee emp : employee)
        {            
            System.out.println(toString(emp));
        }
        System.out.println("---------------------------------------------------------\n\n\n");
    }

    public static void main(String args[])
    {
        ArrayList<Employee> emp = new ArrayList<>();

        emp.add(new Employee("EMP001", "John", "UK", 50000f));
        emp.add(new Employee("EMP002", "Jane", "US", 30000f));
        emp.add(new Employee("EMP003", "Joseph", "France", 40000f));
        emp.add(new Employee("EMP004", "Jonathan", "Canada", 20000f));
        emp.add(new Employee("EMP005", "Jaya", "India", 70000f));


        Employee employee = new Employee();
        employee.show(emp);

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the employee ID to search : ");
        String id = sc.nextLine();

        System.out.print("Enter the employee name to search : ");
        String name = sc.nextLine();

        


        String fetched_data = employee.search(id, name, emp);

        System.out.println(fetched_data);

    }
}